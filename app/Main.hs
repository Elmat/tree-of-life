module Main (main) where

import Lib
import Control.Concurrent
import Control.Monad
import System.Random
import System.Process

type Cell = Int
type Grid = [[Cell]]

width :: Int
width = 40

height :: Int
height = 40

--- utility functiion -> clear console
clear :: IO ()
clear = void $ system "clear"

-- create empty grid
emptyGrid :: Grid
emptyGrid = replicate height (replicate width 0)

-- display cell
showCell :: Cell -> String
showCell 0 = "  "
showCell 1 = " X"

-- display grid
showGrid :: Grid -> String
showGrid = unlines . map (concatMap showCell)

-- create a randomize distribution 
customRandom :: IO Int
customRandom = do
    randomNumber <- randomRIO (1, 5) :: IO Int
    return $ if randomNumber == 1 then 1 else 0

-- generate random grid
randomGrid :: IO Grid
randomGrid = replicateM height $ replicateM width $ customRandom

-- count alive neighbors of a given cell
countNeighbours :: Grid -> Int -> Int -> Int
countNeighbours grid x y = sum [ grid !! i !! j | i <- [x-1..x+1], j <- [y-1..y+1], (i /= x || j /= y) && i >= 0 && j >= 0 && i < height && j < width]

-- game of life rule for one cell
applyRules :: Cell -> Int -> Cell
applyRules cell neighbours
    | cell == 1 && (neighbours < 2 || neighbours > 3) = 0
    | cell == 0 && neighbours == 3 = 1
    | otherwise = cell

-- increment grid 
evolveGrid :: Grid -> Grid
evolveGrid grid = [[applyRules (grid !! i !! j) (countNeighbours grid i j) | j <- [0..width-1]] | i <- [0..height-1]]

-- display grid
renderAndEvolve :: Grid -> IO ()
renderAndEvolve grid = do
    clear
    putStrLn (showGrid grid)
    threadDelay 1000000
    renderAndEvolve (evolveGrid grid)

-- main entrance
main :: IO ()
main = do
    putStrLn "Game of Life - Appuyez sur Ctrl+C pour quitter"
    initialGrid <- randomGrid
    renderAndEvolve initialGrid