# Game of Life

Implémentation d'un jeu de la vie avec Haskell.
Projet généré avec stack (ajout des packages random et process)

~~~
cd game-of-life
stack build
stack exec game-of-life-exe
~~~

Une vidéo de démonstration est présente dans le dossier <code>/video.</code>